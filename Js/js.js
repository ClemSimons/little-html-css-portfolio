window.addEventListener('scroll', () => {
  document.getElementsByClassName('navbar')[0].style.setProperty('opacity', (window.pageYOffset / (document.body.offsetHeight - window.innerHeight) * 8))
  document.querySelectorAll('li')[0].style.setProperty('width', ((10) + window.pageYOffset / (document.body.offsetHeight - window.innerHeight) * 100) + '%')
  document.querySelectorAll('li')[1].style.setProperty('width', ((10) + window.pageYOffset / (document.body.offsetHeight - window.innerHeight) * 100) + '%')
  document.querySelectorAll('li')[2].style.setProperty('width', ((10) + window.pageYOffset / (document.body.offsetHeight - window.innerHeight) * 100) + '%')
  document.getElementsByClassName('main-paragraph-part2-p1')[0].style.setProperty('opacity', (window.pageYOffset / (document.body.offsetHeight - window.innerHeight) * 10))
  document.getElementsByClassName('main-paragraph-part2-p2')[0].style.setProperty('opacity', ((window.pageYOffset / (document.body.offsetHeight - window.innerHeight) * 1.8)))
}, false)
